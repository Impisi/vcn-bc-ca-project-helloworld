class ArticlesController < ApplicationController
  def show
    @article = Article.find(params[:id])
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to articles_path
  end
  
  def index
    @articles = Article.all
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
  end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end

  private
    def article_params
      params.require(:article).permit(:title, :text)
    end
#test to check creation of article
#test to check creation of article with no text
#test to delete article
#test to delete article that doesn't exist
#test to edit article
#test to edit article that doesn't exist
#test to edit article to new id then attempt to delete old id
#test to edit article id to be the same as another article, then delete one
#create an article that hasn't been made yet
end
