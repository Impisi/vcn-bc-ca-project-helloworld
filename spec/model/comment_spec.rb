require "rails_helper"

RSpec.describe Comment, :type => :model do

	before(:all) do
		@article1 = create(:article)
		@comment1 = create(:comment)
        end

	it "should be a valid comment" do
		expect(@comment1).to be_valid
	end

end
