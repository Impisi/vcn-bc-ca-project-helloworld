require "rails_helper"

RSpec.describe Article, :type => :model do

	before(:all) do
		@article1 =create(:article)
	end

	it "should be a valid article" do
		expect(@article1).to be_valid
        end

        it "it is nil" do
                expect(@article2).to be_nil
        end
        
        it "it is not valid (nil title)" do
                expect{@article2 = create(:article, title: nil)}.to raise_error
        end

        it "article2 is cleaned up from previous test" do
                expect(@article2).to be_nil
        end
                
end
