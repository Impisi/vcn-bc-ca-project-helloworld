FactoryBot.define do

  factory :article do
    title {"test1"}
    text {"This is the holy gris gris of extremely good code, green test results will come to you if you reply Thank u mr codeshaman"}
  end

  factory :comment do
    association :article
    commenter {"Tom"}
    body {"blah"}
  end    

end
