require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do
   context "tests index" do
      it "should render index" do
         get :index
         expect(response).to render_template("index")
      end
      it "adds an article and then renders index" do
         article1 = create(:article)
         get :index
         assigns(:articles).should eq(article1)
      end
   end
end
