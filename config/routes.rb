Rails.application.routes.draw do
  get 'helloworld/index'

  resources :articles do
    resources :comments
  end
  
  root 'helloworld#index'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
